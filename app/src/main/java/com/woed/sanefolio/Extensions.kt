package com.woed.sanefolio

import java.lang.IllegalArgumentException
import java.math.BigDecimal
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.time.ZonedDateTime
import java.time.format.DateTimeParseException
import java.util.*

fun Iterable<BigDecimal>.sum(): BigDecimal = fold(BigDecimal.ZERO) { acc, d -> acc + d }

// TODO better way?
// TODO how many max digits?
fun Number.toCurrencyString(currencyCode: String, maximumFractionDigits: Int = 20): String {
    return try {
        NumberFormat.getCurrencyInstance().apply {
            this.maximumFractionDigits = maximumFractionDigits
            currency = Currency.getInstance(currencyCode)
        }.format(this)
    } catch (e: IllegalArgumentException) {
        // TODO max digits?
        try {
            val formatter: DecimalFormat =
                NumberFormat.getCurrencyInstance(Locale.getDefault()) as DecimalFormat
            val symbols: DecimalFormatSymbols = formatter.decimalFormatSymbols
            symbols.currencySymbol = ""
            formatter.decimalFormatSymbols = symbols
            formatter.format(this)
        } catch (e: IllegalArgumentException) {
            "$this ${currencyCode.uppercase()}"
        }
    }
}

fun tryParseZonedDateTime(text: CharSequence) = try {
    ZonedDateTime.parse(text)
} catch (e: DateTimeParseException) {
    // TODO Log
    null
}