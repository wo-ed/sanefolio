package com.woed.sanefolio

import com.woed.sanefolio.storage.Coin
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import java.math.BigDecimal

interface CoinGeckoService {

    @GET("coins/list")
    suspend fun getCoins(): Response<List<Coin>>

    @GET("simple/price")
    suspend fun getPrices(
        @Query("ids") coinIds: String,
        @Query("vs_currencies") vsCurrencies: String,
    ): Response<Map<String, Map<String, BigDecimal>>>
}