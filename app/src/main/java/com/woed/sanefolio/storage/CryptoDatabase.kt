package com.woed.sanefolio.storage

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.firstOrNull

@Database(
    entities = [Coin::class, Transaction::class, IntervalAlarm::class, Price::class],
    version = 1
)
@TypeConverters(RoomTypeConverters::class)
abstract class CryptoDatabase : RoomDatabase() {
    abstract fun cryptoDao(): CryptoDao
}

@Dao
interface CryptoDao {

    // Transaction

    @Insert
    suspend fun insert(transaction: Transaction): Long

    @Delete
    suspend fun deleteTransactions(transactions: List<Transaction>)

    @Query("DELETE FROM `transaction` WHERE buyCoinId = :coinId")
    suspend fun deleteTransactionsByCoinId(coinId: Long)

    @androidx.room.Transaction
    @Query("SELECT * FROM `transaction` ORDER BY timestamp DESC, transactionId DESC")
    fun getTransactionsAndCoins(): Flow<List<TransactionAndCoins>>

    // Coin

    @Insert
    suspend fun insert(coin: Coin): Long

    @Update
    suspend fun update(coin: Coin)

    @androidx.room.Transaction
    suspend fun insertOrUpdate(coin: Coin): Long {
        val oldCoin = getCoin(coin.coinGeckoId).firstOrNull()
        return if (oldCoin != null) {
            coin.coinId = oldCoin.coinId
            update(coin)
            coin.coinId
        } else {
            insert(coin)
        }
    }

    @Query("SELECT * FROM coin ORDER BY name")
    fun getCoins(): Flow<List<Coin>>

    @Query("SELECT * FROM coin WHERE coinGeckoId=:coinGeckoId")
    fun getCoin(coinGeckoId: String?): Flow<Coin?>

    // Price

    @Insert(onConflict = REPLACE)
    suspend fun insert(price: Price): Long

    @Query("SELECT * FROM price WHERE coinGeckoId = :coinGeckoId AND vsCurrency = :vsCurrencySymbol")
    fun getPrice(coinGeckoId: String, vsCurrencySymbol: String): Flow<Price?>

    // IntervalAlarm

    @Insert
    suspend fun insert(alarm: IntervalAlarm): Long

    @Update
    suspend fun update(alarm: IntervalAlarm)

    @Query("SELECT * FROM intervalalarm ORDER BY nextDateTime, intervalAlarmId DESC")
    fun getIntervalAlarms(): Flow<List<IntervalAlarm>>

    @Delete
    suspend fun deleteAlarms(alarms: List<IntervalAlarm>)

    @Delete
    suspend fun delete(alarm: IntervalAlarm)

    @Query("SELECT * FROM intervalalarm WHERE nextDateTime = (SELECT MIN(nextDateTime) FROM intervalalarm)")
    fun getNextAlarm(): Flow<IntervalAlarm?>
}