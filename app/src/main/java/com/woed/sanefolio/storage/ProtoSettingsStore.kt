package com.woed.sanefolio.storage

import android.content.Context
import androidx.datastore.core.CorruptionException
import androidx.datastore.core.DataStore
import androidx.datastore.core.Serializer
import androidx.datastore.dataStore
import androidx.datastore.preferences.protobuf.InvalidProtocolBufferException
import com.woed.sanefolio.ProtoSettings
import java.io.InputStream
import java.io.OutputStream

val Context.protoSettingsStore: DataStore<ProtoSettings> by dataStore(
    fileName = "settings",
    serializer = object : Serializer<ProtoSettings> {

        override val defaultValue = ProtoSettings.getDefaultInstance()

        override suspend fun readFrom(input: InputStream): ProtoSettings {
            try {
                return ProtoSettings.parseFrom(input) // TODO
            } catch (e: InvalidProtocolBufferException) {
                throw CorruptionException("Cannot read proto.", e)
            }
        }

        override suspend fun writeTo(t: ProtoSettings, output: OutputStream) =
            t.writeTo(output) // TODO
    }
)