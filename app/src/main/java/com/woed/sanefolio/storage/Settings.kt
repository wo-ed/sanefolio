package com.woed.sanefolio.storage

import java.time.ZonedDateTime

open class Settings(
    var coinsSyncDate: ZonedDateTime?,
    var pricesSyncDate: ZonedDateTime?,
    var isPortfolioValueVisible: Boolean,
    var isValueInNotificationsEnabled: Boolean,
    var productivityMode: ProductivityMode?,
    var is24HourEnabled: Boolean,
    var vsCurrency: Coin
) {
    val areCoinsUpToDate: Boolean
        get() {
            val coinsSyncDate = this.coinsSyncDate
            return coinsSyncDate != null && ZonedDateTime.now()
                .isBefore(coinsSyncDate.plusDays(COIN_SYNC_INTERVAL_DAYS))
        }

    val isAlarmsModificationAlwaysEnabled
        get() = when (productivityMode) {
            ProductivityMode.MIN, ProductivityMode.MEDIUM -> true
            else -> false
        }

    companion object {
        const val COIN_SYNC_INTERVAL_DAYS = 1L // TODO
    }
}

enum class ProductivityMode {
    MIN,
    MEDIUM,
    MAX
}

object VsCurrencies {
    val usd = Coin("usd", "US Dollar", "FIAT: USD", false)
    val eur = Coin("eur", "Euro", "FIAT: EURO", false)
    val btc = Coin("btc", "Bitcoin", "bitcoin", true)
    val eth = Coin("eth", "Ethereum", "ethereum", true)
    val default get() = usd
    val all = listOf(usd, eur, btc, eth)
    fun getBySymbol(symbol: String) = all.singleOrNull { it.symbol == symbol }
}