package com.woed.sanefolio.storage

import androidx.room.*
import com.google.gson.annotations.SerializedName
import com.woed.sanefolio.REQUEST_CODE_ALARM
import com.woed.sanefolio.ui.LongIdItem
import java.math.BigDecimal
import java.time.ZonedDateTime

@Entity(indices = [Index(value = ["coinGeckoId"], unique = true)])
data class Coin(
    val symbol: String,
    val name: String,

    @SerializedName("id")
    val coinGeckoId: String,

    var isCryptoCurrency: Boolean = true,

    @PrimaryKey(autoGenerate = true)
    var coinId: Long = 0L

) : LongIdItem {
    override val longId get() = coinId
    override fun toString() = "$name (${symbol.uppercase()})"

    // Ignores coinId and longId
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Coin

        if (symbol != other.symbol) return false
        if (name != other.name) return false
        if (coinGeckoId != other.coinGeckoId) return false
        if (isCryptoCurrency != other.isCryptoCurrency) return false

        return true
    }

    // Ignores coinId and longId
    override fun hashCode(): Int {
        var result = symbol.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + coinGeckoId.hashCode()
        result = 31 * result + isCryptoCurrency.hashCode()
        return result
    }
}

@Entity(indices = [Index(value = ["vsCurrency", "coinGeckoId"], unique = true)])
data class Price(
    val value: BigDecimal,
    val vsCurrency: String,

    @SerializedName("id")
    val coinGeckoId: String,

    @PrimaryKey(autoGenerate = true)
    val priceId: Long = 0L
)

@Entity
data class Transaction(
    val buyAmount: BigDecimal,
    var buyCoinId: Long,
    val sellAmount: BigDecimal,
    var sellCoinId: Long,
    val timestamp: ZonedDateTime,

    @PrimaryKey(autoGenerate = true)
    val transactionId: Long = 0L

) : LongIdItem {
    override val longId get() = transactionId
}

data class TransactionAndCoins(
    @Embedded
    val transaction: Transaction,

    @Relation(
        parentColumn = "buyCoinId",
        entityColumn = "coinId"
    )
    val buyCurrency: Coin,

    @Relation(
        parentColumn = "sellCoinId",
        entityColumn = "coinId"
    )
    val sellCurrency: Coin
) : LongIdItem {
    override val longId get() = transaction.longId
}

@Entity
data class IntervalAlarm(
    val intervalDays: Int,
    var nextDateTime: ZonedDateTime,
    val isRepeating: Boolean = true,

    @PrimaryKey(autoGenerate = true)
    var intervalAlarmId: Long = 0L

) : LongIdItem {
    override val longId get() = intervalAlarmId

    val requestCode: Int get() = REQUEST_CODE_ALARM + intervalAlarmId.toInt()

    fun isOverdue() = ZonedDateTime.now().isAfter(nextDateTime)

    fun updateNextDateTime() {
        val now = ZonedDateTime.now()
        while (now.isAfter(nextDateTime)) {
            nextDateTime = nextDateTime.plusDays(intervalDays.toLong())
        }
    }
}