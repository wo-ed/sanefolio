package com.woed.sanefolio.storage

import android.content.Context
import com.woed.sanefolio.ProtoSettings
import com.woed.sanefolio.tryParseZonedDateTime
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

interface SettingsDatabase {
    val settings: Flow<Settings>
    val defaultSettings: Settings
    suspend fun updateSettings(settings: Settings)
}

class ProtoSettingsDatabase(context: Context) : SettingsDatabase {
    private val dataStore = context.protoSettingsStore
    override val settings: Flow<Settings> get() = dataStore.data.map { it.toSettings() }
    override val defaultSettings: Settings get() = ProtoSettings.getDefaultInstance().toSettings()

    override suspend fun updateSettings(settings: Settings) {
        dataStore.updateData {
            it.toBuilder().apply {
                coinsSyncDate = settings.coinsSyncDate.toString()
                pricesSyncDate = settings.pricesSyncDate.toString()
                isPortfolioValueVisible = settings.isPortfolioValueVisible
                isValueInNotificationsEnabled = settings.isValueInNotificationsEnabled
                productivityMode = settings.productivityMode?.name ?: ""
                is24HourEnabled = settings.is24HourEnabled
                vsCurrency = settings.vsCurrency.symbol
            }.build()
        }
    }
}

fun ProtoSettings.toSettings() = Settings(
    coinsSyncDate = tryParseZonedDateTime(coinsSyncDate),
    pricesSyncDate = tryParseZonedDateTime(pricesSyncDate),
    isPortfolioValueVisible = isPortfolioValueVisible,
    isValueInNotificationsEnabled = isValueInNotificationsEnabled,
    productivityMode = productivityMode.ifBlank { null }?.let { ProductivityMode.valueOf(it) },
    is24HourEnabled = is24HourEnabled,
    vsCurrency = VsCurrencies.getBySymbol(vsCurrency) ?: VsCurrencies.default
)