package com.woed.sanefolio.storage

import androidx.room.TypeConverter
import java.math.BigDecimal
import java.time.ZonedDateTime

class RoomTypeConverters {

    @TypeConverter
    fun bigDecimalToString(input: BigDecimal?): String {
        return input?.toPlainString() ?: ""
    }

    @TypeConverter
    fun stringToBigDecimal(input: String?): BigDecimal {
        if (input.isNullOrBlank()) return BigDecimal.valueOf(0.0)
        return input.toBigDecimalOrNull() ?: BigDecimal.valueOf(0.0)
    }

    @TypeConverter
    fun zonedDateTimeToString(input: ZonedDateTime): String = input.toString()

    @TypeConverter
    fun stringToZonedDateTime(input: String): ZonedDateTime = ZonedDateTime.parse(input)
}