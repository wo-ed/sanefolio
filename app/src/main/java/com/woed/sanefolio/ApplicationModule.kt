package com.woed.sanefolio

import android.content.Context
import androidx.room.Room
import com.woed.sanefolio.repositories.CryptoRepository
import com.woed.sanefolio.repositories.SettingsRepository
import com.woed.sanefolio.storage.CryptoDao
import com.woed.sanefolio.storage.CryptoDatabase
import com.woed.sanefolio.storage.ProtoSettingsDatabase
import com.woed.sanefolio.storage.SettingsDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {

    @Provides
    @Singleton
    fun provideCryptoRepository(
        cryptoDao: CryptoDao,
        settingsRepository: SettingsRepository,
        coinGeckoService: CoinGeckoService,
        alarmHandler: IAlarmHandler
    ) = CryptoRepository(cryptoDao, coinGeckoService, settingsRepository, alarmHandler)

    @Provides
    @Singleton
    fun provideCoinGeckoService(): CoinGeckoService = Retrofit.Builder()
        .baseUrl("https://api.coingecko.com/api/v3/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(CoinGeckoService::class.java)

    @Provides
    @Singleton
    fun provideSettingsDatabase(@ApplicationContext context: Context): SettingsDatabase =
        ProtoSettingsDatabase(context)

    @Provides
    @Singleton
    fun provideSettingsRepository(settingsDatabase: SettingsDatabase): SettingsRepository =
        SettingsRepository(settingsDatabase)

    @Provides
    @Singleton
    fun notificationHandler(
        @ApplicationContext context: Context,
        settingsRepository: SettingsRepository
    ) = NotificationHandler(context, settingsRepository)

    @Provides
    @Singleton
    fun alarmHandler(@ApplicationContext context: Context): IAlarmHandler = AlarmHandler(context)

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context) =
        Room.databaseBuilder(context, CryptoDatabase::class.java, "crypto-db").build()

    @Provides
    @Singleton
    fun provideCryptoDao(db: CryptoDatabase) = db.cryptoDao()
}