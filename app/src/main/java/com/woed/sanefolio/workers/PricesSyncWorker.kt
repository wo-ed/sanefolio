package com.woed.sanefolio.workers

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.*
import com.woed.sanefolio.NotificationHandler
import com.woed.sanefolio.repositories.CryptoRepository
import com.woed.sanefolio.repositories.SettingsRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.take

@HiltWorker
class PricesSyncWorker @AssistedInject constructor(
    @Assisted context: Context,
    @Assisted workerParams: WorkerParameters,
    private val cryptoRepository: CryptoRepository,
    private val notificationHandler: NotificationHandler,
    private val settingsRepository: SettingsRepository
) : CoroutineWorker(context, workerParams) {

    // TODO Error Handling
    override suspend fun doWork() = try {
        val isManualSync = inputData.getBoolean(PARAM_IS_MANUAL_SYNC, false)
        if (isManualSync) {
            doManualWork()
        } else {
            doAlarmWork()
        }

        Result.success()
    } catch (e: Exception) {
        // TODO Logging
        notificationHandler.sendPortfolioValueError()
        Result.failure()
    }

    private suspend fun doManualWork() {
        cryptoRepository.synchronizePrices()
    }

    private suspend fun doAlarmWork() {
        cryptoRepository.getIntervalAlarms().take(1).collect { alarms ->
            val overdueAlarms = alarms.filter { it.isOverdue() }
            if (overdueAlarms.isNotEmpty()) {

                cryptoRepository.synchronizePrices()
                showNotification()

                // Reschedule overdue alarms
                overdueAlarms.forEach {
                    if (it.isRepeating) {
                        cryptoRepository.rescheduleAlarm(it)
                    } else {
                        cryptoRepository.delete(it)
                    }
                }
            }
        }
    }

    private suspend fun showNotification() {
        val isValueInNotificationEnabled =
            settingsRepository.getSettingsInstance().isValueInNotificationsEnabled

        if (isValueInNotificationEnabled) {
            val totalPortfolioValue = cryptoRepository.getTotalPortfolioValue().first()
            notificationHandler.sendPortfolioValueNotification(totalPortfolioValue)
        } else {
            notificationHandler.sendSecretPortfolioValueNotification()
        }
    }

    companion object {
        const val PARAM_IS_MANUAL_SYNC = "PARAM_IS_MANUAL_SYNC"
        const val UNIQUE_WORK_SYNC_PRICES = "SYNC_PRICES"

        fun runUnique(context: Context, isManualSync: Boolean = false): Operation {
            val data = workDataOf(PARAM_IS_MANUAL_SYNC to isManualSync)
            val workRequest =
                OneTimeWorkRequestBuilder<PricesSyncWorker>().setInputData(data).build()

            return WorkManager.getInstance(context)
                .enqueueUniqueWork(UNIQUE_WORK_SYNC_PRICES, ExistingWorkPolicy.KEEP, workRequest)
        }
    }
}