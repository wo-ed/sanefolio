package com.woed.sanefolio.workers

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.lifecycle.LiveData
import androidx.work.*
import com.woed.sanefolio.NotificationHandler
import com.woed.sanefolio.repositories.CryptoRepository
import com.woed.sanefolio.repositories.SettingsRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject

@HiltWorker
class CoinsSyncWorker @AssistedInject constructor(
    @Assisted context: Context,
    @Assisted workerParams: WorkerParameters,
    private val cryptoRepository: CryptoRepository,
    private val settingsRepository: SettingsRepository,
    private val notificationHandler: NotificationHandler
) : CoroutineWorker(context, workerParams) {

    // TODO Retry, Error Handling
    override suspend fun doWork() = try {
        val settings = settingsRepository.getSettingsInstance()
        if (!settings.areCoinsUpToDate) {
            cryptoRepository.synchronizeCoins { progress ->
                notificationHandler.sendCoinsSyncProgressNotification(
                    progress.times(100).toInt()
                )
            }
            notificationHandler.cancelCoinSyncNotification()
        }

        Result.success()
    } catch (e: Exception) {
        // TODO Logging
        notificationHandler.cancelCoinSyncNotification()
        Result.failure()
    }

    companion object {
        const val UNIQUE_WORK_SYNC_COINS = "SYNC_COINS"

        fun runUnique(context: Context): Operation {
            val workRequest =
                OneTimeWorkRequestBuilder<CoinsSyncWorker>()
                    //.setInputData(workDataOf())
                    .build()

            return WorkManager.getInstance(context)
                .enqueueUniqueWork(UNIQUE_WORK_SYNC_COINS, ExistingWorkPolicy.KEEP, workRequest)
        }

        fun getWorkInfosForUniqueWorkLiveData(context: Context): LiveData<MutableList<WorkInfo>> =
            WorkManager.getInstance(context)
                .getWorkInfosForUniqueWorkLiveData(CoinsSyncWorker.UNIQUE_WORK_SYNC_COINS)
    }
}