package com.woed.sanefolio

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.woed.sanefolio.repositories.SettingsRepository
import com.woed.sanefolio.ui.main.MainActivity
import java.math.BigDecimal

class NotificationHandler(
    private val context: Context,
    private val settingsRepository: SettingsRepository
) {
    fun sendCoinsSyncProgressNotification(progressPercent: Int) {
        val builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannels.synchronization
            NotificationCompat.Builder(context, channel.id)
                .setPriority(channel.importance)
        } else {
            NotificationCompat.Builder(context, "") // TODO
                .setPriority(NotificationManager.IMPORTANCE_DEFAULT)
        }.apply {
            setDefaults(Notification.DEFAULT_LIGHTS or Notification.DEFAULT_SOUND)
            setSmallIcon(R.drawable.ic_baseline_account_balance_24)
            setContentTitle("Synchronizing cryptocurrencies") // TODO
            setAutoCancel(true)
            setVibrate(longArrayOf()) // TODO
            setOnlyAlertOnce(true)
            setContentText("$progressPercent%")
            setProgress(100, progressPercent, false)
        }

        with(NotificationManagerCompat.from(context)) {
            notify(NOTIFICATION_COINS_SYNC, builder.build())
        }
    }

    fun cancelCoinSyncNotification() {
        with(NotificationManagerCompat.from(context)) {
            cancel(NOTIFICATION_COINS_SYNC)
        }
    }

    fun sendSecretPortfolioValueNotification() {
        sendPortfolioValueNotification(context.getString(R.string.secret_portfolio_notification))
    }

    suspend fun sendPortfolioValueNotification(portfolioValue: BigDecimal) {
        val settings = settingsRepository.getSettingsInstance()
        val message = context.getString(
            R.string.portfolio_value_notification,
            portfolioValue.toCurrencyString(settings.vsCurrency.symbol)
        )
        sendPortfolioValueNotification(message)
    }

    private fun sendPortfolioValueNotification(message: String) {
        val builder = portfolioValueNotificationBuilder().setContentText(message)
        with(NotificationManagerCompat.from(context)) {
            notify(NOTIFICATION_PRICES_SYNC, builder.build())
        }
    }

    fun sendPortfolioValueError() {
        val builder = portfolioValueNotificationBuilder()
            .setContentText("Couldn't fetch portfolio value")
            .addAction(
                R.drawable.ic_baseline_refresh_24, "Retry",
                MyBroadcastReceiver.createBroadcastPendingIntent(
                    context, REQUEST_CODE_RETRY, ACTION_PRICES_SYNC_RETRY
                )
            )

        with(NotificationManagerCompat.from(context)) {
            notify(NOTIFICATION_PRICES_SYNC, builder.build())
        }
    }

    private fun portfolioValueNotificationBuilder(): NotificationCompat.Builder {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannels.updates
            NotificationCompat.Builder(context, channel.id)
                .setPriority(channel.importance)
        } else {
            NotificationCompat.Builder(context, "") // TODO
                .setPriority(NotificationManager.IMPORTANCE_DEFAULT)
        }.apply {
            setSmallIcon(R.drawable.ic_baseline_account_balance_24)
            setContentTitle(context.getString(R.string.app_name)) // TODO
            setAutoCancel(true)
            setContentIntent(PendingIntent.getActivity(context, REQUEST_CODE_START_MAIN, Intent(context, MainActivity::class.java), PendingIntent.FLAG_IMMUTABLE))
        }
    }

    companion object {
        const val NOTIFICATION_COINS_SYNC = 1
        const val NOTIFICATION_PRICES_SYNC = 2
    }
}

@RequiresApi(Build.VERSION_CODES.O)
object NotificationChannels {

    val updates =
        NotificationChannel("UPDATES", "Prices Updates", NotificationManager.IMPORTANCE_DEFAULT)
            .apply { description = "Show the price updates" }

    val synchronization = NotificationChannel(
        "SYNCHRONIZATION",
        "Synchronization",
        NotificationManager.IMPORTANCE_DEFAULT
    ).apply {
        description = "Show the synchronization progress"
    }

    private val allChannels = listOf(updates, synchronization)

    fun Context.createNotificationChannels() {
        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        allChannels.forEach {
            manager.createNotificationChannel(it)
        }
    }
}