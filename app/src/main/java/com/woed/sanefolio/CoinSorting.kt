package com.woed.sanefolio

import com.woed.sanefolio.storage.Coin

fun filterCoinsSorted(query: String, coins: List<Coin>): List<Coin> {
    if (query.isBlank()) {
        return emptyList()
    }

    val queryParts = query.split(' ')

    val filteredCoins = coins.filter { coin ->
        queryParts.any {
            coin.name.contains(it, true) || coin.symbol.contains(it, true)
        }
    }

    val sortedCoins = filteredCoins.sortedWith { c1, c2 ->
        compareCoinsByQueryEquality(c2, c1, query).takeUnless { it == 0 }
            ?: compareCoinsByQueryPartsMatching(c2, c1, queryParts).takeUnless { it == 0 }
            ?: compareCoinsByQueryPartsStart(c2, c1, queryParts).takeUnless { it == 0 }
            ?: compareCoinsByQueryPartsContaining(c2, c1, queryParts)
    }

    return sortedCoins
}

// Compare if the query matches the name or ticker
private fun compareCoinsByQueryEquality(c2: Coin, c1: Coin, query: String): Int {
    return compareValuesBy(c2, c1) { coin ->
        coin.attr.count { it.equals(query, true) }
    }
}

// Compare how many parts of the query match the name or ticker
private fun compareCoinsByQueryPartsMatching(c2: Coin, c1: Coin, queryParts: List<String>): Int {
    return compareValuesBy(c2, c1) { coin ->
        queryParts.sumOf { part -> coin.attr.count { it.equals(part, true) } }
    }
}

// Compare how many parts of the query are the start of the name or ticker
private fun compareCoinsByQueryPartsStart(c2: Coin, c1: Coin, queryParts: List<String>): Int {
    return compareValuesBy(c2, c1) { coin ->
        queryParts.sumOf { part -> coin.attr.count { it.startsWith(part, true) } }
    }
}

// Compare how many parts of the query are part of the name or ticker
private fun compareCoinsByQueryPartsContaining(c2: Coin, c1: Coin, queryParts: List<String>): Int {
    return compareValuesBy(c2, c1) { coin ->
        queryParts.sumOf { part -> coin.attr.count { it.contains(part, true) } }
    }
}

private val Coin.attr get() = listOf(name, symbol)