package com.woed.sanefolio

import com.woed.sanefolio.storage.Coin
import com.woed.sanefolio.ui.LongIdItem
import java.math.BigDecimal

data class Asset(
    val coin: Coin,
    var amount: BigDecimal,
    val lastPrice: BigDecimal?,
    val lastPriceCurrency: Coin
) : LongIdItem {
    override val longId get() = coin.longId
    val totalValue: BigDecimal? get() = lastPrice?.times(amount)
}