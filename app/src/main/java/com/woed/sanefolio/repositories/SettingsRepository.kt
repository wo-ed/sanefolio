package com.woed.sanefolio.repositories

import com.woed.sanefolio.storage.Settings
import com.woed.sanefolio.storage.SettingsDatabase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.firstOrNull

class SettingsRepository(private val db: SettingsDatabase) {
    val settings: Flow<Settings> get() = db.settings
    private val defaultSettings: Settings get() = db.defaultSettings

    suspend fun getSettingsInstance(): Settings = settings.firstOrNull() ?: defaultSettings

    suspend fun updateSettings(edit: (Settings) -> Unit) {
        val settings = getSettingsInstance()
        edit(settings)
        db.updateSettings(settings)
    }
}