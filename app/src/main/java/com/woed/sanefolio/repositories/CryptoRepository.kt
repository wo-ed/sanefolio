package com.woed.sanefolio.repositories

import com.woed.sanefolio.*
import com.woed.sanefolio.storage.*
import kotlinx.coroutines.flow.*
import retrofit2.Response
import java.math.BigDecimal
import java.time.ZonedDateTime

// TODO Move alarm code to a new class: AlarmRepository?
class CryptoRepository(
    private val cryptoDao: CryptoDao,
    private val coinGeckoService: CoinGeckoService,
    private val settingsRepository: SettingsRepository,
    private val alarmHandler: IAlarmHandler
) {
    suspend fun getCoins(): List<Coin>? {
        val dbCoins = cryptoDao.getCoins().firstOrNull()
        val areCoinsUpToDate = settingsRepository.getSettingsInstance().areCoinsUpToDate
        val cryptos = if (areCoinsUpToDate && dbCoins?.isNotEmpty() == true) {
            dbCoins
        } else {
            getCoinsRemote().body()
        }
        return cryptos?.let { VsCurrencies.all.plus(it).distinct() }
    }

    suspend fun deleteTransactionByCoinId(id: Long) {
        cryptoDao.deleteTransactionsByCoinId(id)
    }

    suspend fun deleteTransactions(transactions: List<Transaction>) {
        cryptoDao.deleteTransactions(transactions)
    }

    fun getAssets(): Flow<List<Asset>> {
        return getTransactionsAndCoins().flatMapLatest { transactionsAndCoins ->

            //val coinGeckoIds = transactionsAndCoins.flatMap { listOf(it.fromCurrency, it.toCurrency) }.distinct()
            val buyCurrencies = transactionsAndCoins.map { it.buyCurrency }.distinct()

            val assetFlows: List<Flow<Asset>> = buyCurrencies.map { buyCurrency ->
                settingsRepository.settings.flatMapLatest { settings ->
                    getPrice(buyCurrency, settings.vsCurrency).map { price ->
                        val addedAmounts = transactionsAndCoins.filter {
                            it.buyCurrency == buyCurrency
                        }.map { it.transaction.buyAmount }

                        val removedAmounts = transactionsAndCoins.filter {
                            it.sellCurrency == buyCurrency
                        }.map { it.transaction.sellAmount }

                        val amount = addedAmounts.sum() - removedAmounts.sum()
                        Asset(buyCurrency, amount, price?.value, settings.vsCurrency)
                    }
                }
            }

            combine(assetFlows) { assets ->
                assets.sortedBy { it.coin.name }
                    .sortedByDescending { it.totalValue }
            }.onEmpty { emit(emptyList()) }
        }
    }

    // TODO Allow non-crypto prices
    private fun getPrice(currency: Coin, vsCurrency: Coin): Flow<Price?> {
        val coinGeckoId = currency.coinGeckoId
        return if (!currency.isCryptoCurrency) {
            flow { emit(null) }
        } else {
            cryptoDao.getPrice(coinGeckoId, vsCurrency.symbol)
        }
    }

    fun isManualPricesSyncAllowed() = settingsRepository.settings.map {
        it.productivityMode == ProductivityMode.MIN && it.isPortfolioValueVisible
    }

    private suspend fun insertOrUpdate(coin: Coin): Coin =
        coin.apply { coinId = cryptoDao.insertOrUpdate(coin) }

    // Returns the stored transaction with id
    suspend fun insertOrUpdate(transactionAndCoins: TransactionAndCoins): TransactionAndCoins =
        transactionAndCoins.apply {
            val buyCurrencyId = insertOrUpdate(transactionAndCoins.buyCurrency).coinId
            val sellCurrencyId = insertOrUpdate(transactionAndCoins.sellCurrency).coinId
            transaction.buyCoinId = buyCurrencyId
            transaction.sellCoinId = sellCurrencyId
            cryptoDao.insert(transactionAndCoins.transaction)
        }

    fun getTransactionsAndCoins(): Flow<List<TransactionAndCoins>> =
        cryptoDao.getTransactionsAndCoins()

    private suspend fun getCoinsRemote(): Response<List<Coin>> = coinGeckoService.getCoins().apply {
        body()?.map { it.isCryptoCurrency = true }
    }

    suspend fun synchronizeCoins(progressListener: ((Float) -> Unit)? = null) {
        val response = getCoinsRemote()
        settingsRepository.updateSettings {
            it.coinsSyncDate = null
        }
        response.body()?.also { coins ->
            val coinsCount = coins.size.toFloat()
            coins.forEachIndexed { i, coin ->
                insertOrUpdate(coin)
                progressListener?.invoke((i + 1) / coinsCount)
            }
            settingsRepository.updateSettings {
                it.coinsSyncDate = ZonedDateTime.now()
            }
        }
    }

    suspend fun synchronizePrices() {
        getTransactionsAndCoins().take(1).collect { transactions ->
            val coinGeckoIds = transactions.map { it.buyCurrency.coinGeckoId }.distinct()
            coinGeckoService.getPrices(
                coinGeckoIds.joinToString(","),
                VsCurrencies.all.joinToString(",") { it.symbol }
            ).body()?.onEach { (coinGeckoId, priceMap) ->
                priceMap.forEach { (vsCurrency, value) ->
                    cryptoDao.insert(Price(value, vsCurrency, coinGeckoId))
                }
            }
            settingsRepository.updateSettings {
                it.pricesSyncDate = ZonedDateTime.now()
            }
        }
    }

    fun getTotalPortfolioValue(): Flow<BigDecimal> {
        return getAssets().map { assets -> assets.map { it.totalValue ?: BigDecimal.ZERO }.sum() }
    }

    // TODO Rename because scheduleAlarm sets up the alarm
    suspend fun rescheduleAlarm(alarm: IntervalAlarm) {
        alarm.updateNextDateTime()
        update(alarm)
    }

    // Returns the stored alarm with id
    private suspend fun insertOrUpdate(alarm: IntervalAlarm): IntervalAlarm =
        alarm.apply { intervalAlarmId = cryptoDao.insert(this) }

    suspend fun scheduleAlarm(alarm: IntervalAlarm) {
        alarmHandler.setupAlarm(insertOrUpdate(alarm))
    }

    private suspend fun update(alarm: IntervalAlarm) {
        cryptoDao.update(alarm)
    }

    fun getIntervalAlarms(): Flow<List<IntervalAlarm>> = cryptoDao.getIntervalAlarms()

    suspend fun delete(alarm: IntervalAlarm) {
        alarmHandler.stopAlarm(alarm)
        cryptoDao.delete(alarm)
    }

    fun getNextAlarm(): Flow<IntervalAlarm?> = cryptoDao.getNextAlarm()

    suspend fun setupStoredAlarms() {
        getIntervalAlarms().take(1).collect { alarms ->
            alarms.forEach { alarmHandler.setupAlarm(it) }
        }
    }

    suspend fun deleteAlarms(alarms: List<IntervalAlarm>) {
        cryptoDao.deleteAlarms(alarms)
    }
}