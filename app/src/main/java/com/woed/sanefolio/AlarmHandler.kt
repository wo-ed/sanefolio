package com.woed.sanefolio

import android.app.AlarmManager
import android.content.Context
import com.woed.sanefolio.storage.IntervalAlarm

interface IAlarmHandler {
    fun setupAlarm(alarm: IntervalAlarm)
    fun stopAlarm(alarm: IntervalAlarm)
}

class AlarmHandler(private val context: Context) : IAlarmHandler {

    override fun setupAlarm(alarm: IntervalAlarm) {
        val alarmManager = (context.getSystemService(Context.ALARM_SERVICE) as AlarmManager)
        val pendingIntent = MyBroadcastReceiver.createBroadcastPendingIntent(
            context,
            alarm.requestCode,
            ACTION_ALARM
        )
        alarmManager.setInexactRepeating(
            AlarmManager.RTC_WAKEUP,
            alarm.nextDateTime.toInstant().toEpochMilli(),
            AlarmManager.INTERVAL_DAY,
            pendingIntent
        )
    }

    override fun stopAlarm(alarm: IntervalAlarm) {
        val alarmManager = (context.getSystemService(Context.ALARM_SERVICE) as AlarmManager)
        val pendingIntent = MyBroadcastReceiver.createBroadcastPendingIntent(
            context,
            alarm.requestCode,
            ACTION_ALARM
        )
        alarmManager.cancel(pendingIntent)
    }
}