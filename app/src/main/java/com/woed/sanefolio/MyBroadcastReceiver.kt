package com.woed.sanefolio

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.woed.sanefolio.repositories.CryptoRepository
import com.woed.sanefolio.workers.PricesSyncWorker
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class MyBroadcastReceiver : BroadcastReceiver() {

    @Inject
    lateinit var cryptoRepository: CryptoRepository

    override fun onReceive(context: Context, intent: Intent) {
        when (intent.action) {
            Intent.ACTION_BOOT_COMPLETED,
            Intent.ACTION_MY_PACKAGE_REPLACED,
            -> setupStoredAlarms()
            ACTION_ALARM -> PricesSyncWorker.runUnique(context, false)
            ACTION_PRICES_SYNC_RETRY -> PricesSyncWorker.runUnique(context, false)
            else -> { /* TODO Log */
            }
        }
    }

    private fun setupStoredAlarms() {
        // TODO Replace GlobalScope
        GlobalScope.launch {
            cryptoRepository.setupStoredAlarms()
        }
    }

    companion object {
        fun createBroadcastPendingIntent(
            context: Context,
            requestCode: Int,
            action: String
        ): PendingIntent = PendingIntent.getBroadcast(
            context,
            requestCode,
            Intent(action, null, context, MyBroadcastReceiver::class.java),
            PendingIntent.FLAG_UPDATE_CURRENT.or(PendingIntent.FLAG_IMMUTABLE)
        )
    }
}