package com.woed.sanefolio.ui

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.text.Editable
import android.widget.EditText
import androidx.core.widget.doAfterTextChanged
import java.time.LocalDate
import java.time.LocalTime
import java.time.ZoneId
import java.time.ZonedDateTime

fun Context.dateTimePicker(
    initial: ZonedDateTime,
    is24HourView: Boolean = true,
    callback: (ZonedDateTime) -> Unit
) {
    datePicker(initial.toLocalDate()) { date ->
        timePicker(initial.toLocalTime(), is24HourView) { time ->
            callback(ZonedDateTime.of(date, time, ZoneId.systemDefault()))
        }
    }
}

fun Context.datePicker(initial: LocalDate, callback: (LocalDate) -> Unit) {
    DatePickerDialog(this, { _, year, month, dayOfMonth ->
        callback(LocalDate.of(year, month + 1, dayOfMonth))
    }, initial.year, initial.monthValue - 1, initial.dayOfMonth).apply { show() }
}

fun Context.timePicker(
    initial: LocalTime,
    is24HourView: Boolean = true,
    callback: (LocalTime) -> Unit
) {
    TimePickerDialog(this, { _, hour, minute ->
        callback(LocalTime.of(hour, minute))
    }, initial.hour, initial.minute, is24HourView).apply { show() }
}

fun EditText.onlyPositiveDecimals() {
    doAfterTextChanged {
        it?.replaceIfContainsMatch("[^\\d.]".toRegex(), "") // Only digits and dots
        //it?.replaceIfContainsMatch("\\.\\.".toRegex(), ".")

        // Only one dot
        if (it != null && it.count { c -> c == '.' } > 1 && it.last() == '.') {
            val new = it.dropLast(1)
            it.clear()
            it.append(new)
        }
    }
}

fun EditText.onlyPositiveNonZeroIntegers() {
    doAfterTextChanged {
        it?.replaceIfContainsMatch("\\D".toRegex(), "") // Only digits
        it?.replaceIfContainsMatch("^0+".toRegex(), "") // No leading zero
    }
}

fun Editable.replaceIfContainsMatch(regex: Regex, replacement: String) {
    val input = this.toString()
    if (regex.containsMatchIn(input)) {
        clear()
        append(regex.replace(input, replacement))
    }
}