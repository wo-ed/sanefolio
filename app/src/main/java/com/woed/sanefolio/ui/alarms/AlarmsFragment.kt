package com.woed.sanefolio.ui.alarms

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.woed.sanefolio.R
import com.woed.sanefolio.databinding.FragmentAlarmsBinding
import com.woed.sanefolio.storage.IntervalAlarm
import com.woed.sanefolio.ui.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AlarmsFragment : Fragment() {

    private val viewModel: AlarmsViewModel by viewModels()
    private var _binding: FragmentAlarmsBinding? = null
    private val binding get() = _binding!!

    private val args: AlarmsFragmentArgs by navArgs()

    private var actionModeManager: ActionModeManager<IntervalAlarm>? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_alarms, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        viewModel.isEditOnce = args.isEditOnce

        val adapter = AlarmsAdapter(requireContext())
        binding.alarmsListView.adapter = adapter
        binding.alarmsListView.layoutManager = LinearLayoutManager(requireContext())

        binding.dateButton.setOnClickListener {
            val initial = viewModel.dateTime.value!!
            // TODO 24Hour false doesn't work
            requireContext().dateTimePicker(initial, viewModel.is24HourEnabled.value ?: true) {
                viewModel.updateDateTime(it)
            }
        }

        binding.createAlarmButton.setOnClickListener {
            createAlarmFromInput()?.let { viewModel.saveAlarm(it) }
        }

        adapter.registerAdapterDataObserver(
            object : RecyclerView.AdapterDataObserver() {
                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    binding.alarmsListView.scrollToPosition(positionStart)
                }
            }
        )

        viewModel.alarms.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }

        binding.intervalInput.onlyPositiveNonZeroIntegers()

        binding.saveAlarmsButton.setOnClickListener {
            if (viewModel.alarms.value?.any() == true) {
                val action = AlarmsFragmentDirections.actionNavAlarmsToNavPortfolio()
                findNavController().navigate(action)
            }
        }

        viewModel.isModificationEnabled.observe(viewLifecycleOwner) { isAlarmsModificationEnabled ->
            if (isAlarmsModificationEnabled) {
                val selectionTracker = longSelectionTrackerBuilder(
                    SELECTION_ALARMS, binding.alarmsListView
                ).build().also { adapter.selectionTracker = it }

                actionModeManager =
                    ActionModeManager(object : ActionModeManager.Provider<IntervalAlarm> {
                        override val selectionTracker: SelectionTracker<Long> get() = selectionTracker
                        override fun getActivity() = this@AlarmsFragment.activity
                        override fun getAllItems() = viewModel.alarms.value ?: emptyList()
                        override fun getItemById(id: Long): IntervalAlarm? =
                            viewModel.alarms.value?.firstOrNull { it.longId == id }

                        override fun deleteItems(items: List<IntervalAlarm>) {
                            viewModel.deleteAlarms(items)
                        }
                    }).apply { create() }

            } else {
                actionModeManager?.destroy()
                actionModeManager = null
                adapter.selectionTracker = null
            }
        }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        actionModeManager?.destroy()
        actionModeManager = null
        _binding = null
    }

    private fun createAlarmFromInput(): IntervalAlarm? {
        val intervalDays = binding.intervalInput.text.toString().toIntOrNull() ?: 1
        val dateTime = viewModel.dateTime.value
        val isRepeating = binding.repeatCheckbox.isChecked

        return if (dateTime != null) {
            IntervalAlarm(intervalDays, dateTime, isRepeating)
        } else null
    }
}