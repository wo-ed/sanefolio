package com.woed.sanefolio.ui.modes

enum class InitialAction {
    SHOW_MODE_SELECTION,
    SHOW_ALARM_SELECTION,
    SHOW_PORTFOLIO
}