package com.woed.sanefolio.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.woed.sanefolio.R
import com.woed.sanefolio.databinding.FragmentSettingsBinding
import com.woed.sanefolio.storage.VsCurrencies
import com.woed.sanefolio.storage.Coin
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SettingsFragment : Fragment() {
    private val viewModel: SettingsViewModel by viewModels()
    private var _binding: FragmentSettingsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        binding.vsCurrencySpinner.adapter = ArrayAdapter(
            requireContext(),
            R.layout.list_item_coin, // TODO
            R.id.coinNameView, // TODO
            VsCurrencies.all
        )

        binding.vsCurrencySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val vsCurrency  = parent?.getItemAtPosition(position) as? Coin
                vsCurrency?.let { viewModel.setVsCurrency(it) }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        viewModel.vsCurrency.observe(viewLifecycleOwner) {
            binding.vsCurrencySpinner.setSelection(VsCurrencies.all.indexOf(viewModel.vsCurrency.value))
        }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}