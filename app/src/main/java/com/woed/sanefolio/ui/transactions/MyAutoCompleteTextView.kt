package com.woed.sanefolio.ui.transactions

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import com.google.android.material.textfield.MaterialAutoCompleteTextView

class MyAutoCompleteTextView : MaterialAutoCompleteTextView {
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr)

    override fun replaceText(text: CharSequence?) {
        // Do not replace the text at all before onItemClick
        // This is needed because the updated text changes the filtering result
    }
}