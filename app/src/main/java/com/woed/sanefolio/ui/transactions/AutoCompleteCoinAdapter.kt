package com.woed.sanefolio.ui.transactions

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import com.woed.sanefolio.R
import com.woed.sanefolio.filterCoinsSorted
import com.woed.sanefolio.storage.Coin

class AutoCompleteCoinAdapter(context: Context, var allCoins: List<Coin> = emptyList()) :
    ArrayAdapter<Coin>(context, R.layout.list_item_coin) {

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                this@AutoCompleteCoinAdapter.apply {
                    clear()
                    addAll(filterResults.values as? List<Coin> ?: emptyList())
                }
            }

            override fun performFiltering(query: CharSequence?) = FilterResults().apply {
                values = filterCoinsSorted(query?.toString() ?: "", allCoins)
            }
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(context).inflate(R.layout.list_item_coin, parent, false)
        val coin = getItem(position)
        view?.findViewById<TextView>(R.id.coinNameView)?.text = coin?.name ?: ""
        view?.findViewById<TextView>(R.id.symbolView)?.text = coin?.symbol?.uppercase() ?: ""
        return view
    }
}