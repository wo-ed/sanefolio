package com.woed.sanefolio.ui.portfolio

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.woed.sanefolio.Asset
import com.woed.sanefolio.repositories.CryptoRepository
import com.woed.sanefolio.repositories.SettingsRepository
import com.woed.sanefolio.sum
import com.woed.sanefolio.toCurrencyString
import com.woed.sanefolio.ui.dateTimeFormatter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.launch
import java.math.BigDecimal
import javax.inject.Inject

@HiltViewModel
class PortfolioViewModel @Inject constructor(
    private val cryptoRepository: CryptoRepository,
    private val settingsRepository: SettingsRepository,
) : ViewModel() {

    val assets: LiveData<List<Asset>> = cryptoRepository.getAssets().asLiveData()

    val lastUpdate: LiveData<String?> = settingsRepository.settings.map {
        it.pricesSyncDate?.format(dateTimeFormatter)
    }.asLiveData()

    val nextUpdate: LiveData<String?> = cryptoRepository.getNextAlarm().map {
        it?.nextDateTime?.format(dateTimeFormatter)
    }.asLiveData()

    val isManualPricesSyncAllowed: LiveData<Boolean> =
        cryptoRepository.isManualPricesSyncAllowed().asLiveData()

    val totalPortfolioValue: LiveData<String> =
        cryptoRepository.getAssets().flatMapLatest { assets ->
            settingsRepository.settings.map { settings ->
                assets.map { asset -> asset.totalValue ?: BigDecimal.ZERO }.sum()
                    .toCurrencyString(settings.vsCurrency.symbol)
            }
        }.asLiveData()

    val isPortfolioValueVisible =
        settingsRepository.settings.map { it.isPortfolioValueVisible }.asLiveData()

    init {
        synchronizePricesIfAllowed()
    }

    fun synchronizePricesIfAllowed() {
        viewModelScope.launch {
            cryptoRepository.isManualPricesSyncAllowed().take(1).collect { isAllowed ->
                if (isAllowed) {
                    cryptoRepository.synchronizePrices()
                }
            }
        }
    }

    fun togglePortfolioValueVisibility() {
        viewModelScope.launch {
            settingsRepository.updateSettings {
                it.isPortfolioValueVisible = !it.isPortfolioValueVisible
            }
        }
    }

    fun deleteAssets(assets: List<Asset>) {
        viewModelScope.launch {
            assets.forEach {
                cryptoRepository.deleteTransactionByCoinId(it.coin.coinId)
            }
        }
    }
}