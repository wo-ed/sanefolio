package com.woed.sanefolio.ui

import java.time.format.DateTimeFormatter

const val SELECTION_ASSETS = "assets"
const val SELECTION_TRANSACTIONS = "transactions"
const val SELECTION_ALARMS = "alarms"

val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd. MMMM yyyy HH:mm")