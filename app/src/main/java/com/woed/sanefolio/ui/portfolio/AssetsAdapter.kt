package com.woed.sanefolio.ui.portfolio

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.woed.sanefolio.Asset
import com.woed.sanefolio.R
import com.woed.sanefolio.databinding.ListItemAssetBinding
import com.woed.sanefolio.toCurrencyString
import com.woed.sanefolio.ui.IdItemListAdapter

class AssetsAdapter(
    private val context: Context,
    private val viewLifecycleOwner: LifecycleOwner,
    private val viewModelStoreOwner: ViewModelStoreOwner
) : IdItemListAdapter<Asset, AssetsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ListItemAssetBinding>(
            LayoutInflater.from(context),
            R.layout.list_item_asset,
            parent,
            false
        )

        // TODO Is this the correct way?
        val itemViewModel =
            ViewModelProvider(viewModelStoreOwner)[AssetItemViewModel::class.java]
        binding.lifecycleOwner = viewLifecycleOwner
        binding.itemViewModel = itemViewModel

        return ViewHolder(itemViewModel, binding)
    }

    inner class ViewHolder(
        private val itemViewModel: AssetItemViewModel,
        private val binding: ListItemAssetBinding
    ) : IdItemListAdapter.ViewHolder<Asset>(binding.root) {

        override fun bind(item: Asset, isActivated: Boolean) {
            super.bind(item, isActivated)

            binding.assetValueView.text = if (item.coin.isCryptoCurrency) {
                item.totalValue?.toCurrencyString(item.lastPriceCurrency.symbol) ?: "???" // TODO
            } else {
                ""
            }

            binding.apply {
                coinNameView.text = item.coin.name
                amountView.text = item.amount.toCurrencyString(item.coin.symbol)
                coinPriceView.text = item.lastPrice
                    ?.toCurrencyString(item.lastPriceCurrency.symbol) ?: "???" // TODO
            }
        }

        override fun setViewActivated(isActivated: Boolean) {
            binding.assetContentView.isActivated = isActivated
        }
    }
}