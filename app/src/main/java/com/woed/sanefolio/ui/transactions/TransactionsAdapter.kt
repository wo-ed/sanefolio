package com.woed.sanefolio.ui.transactions

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.woed.sanefolio.R
import com.woed.sanefolio.databinding.ListItemAssetBinding
import com.woed.sanefolio.databinding.ListItemTransactionBinding
import com.woed.sanefolio.storage.TransactionAndCoins
import com.woed.sanefolio.toCurrencyString
import com.woed.sanefolio.ui.IdItemListAdapter
import com.woed.sanefolio.ui.dateTimeFormatter

class TransactionsAdapter(private val context: Context) :
    IdItemListAdapter<TransactionAndCoins, TransactionsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ListItemTransactionBinding>(
            LayoutInflater.from(context),
            R.layout.list_item_transaction,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    inner class ViewHolder(private val binding: ListItemTransactionBinding) :
        IdItemListAdapter.ViewHolder<TransactionAndCoins>(binding.root) {

        override fun bind(item: TransactionAndCoins, isActivated: Boolean) {
            super.bind(item, isActivated)

            binding.apply {
                dateView.text = item.transaction.timestamp.format(dateTimeFormatter)
                buyCurrencyName.text = item.buyCurrency.name
                sellCurrencyName.text = item.sellCurrency.name
                buyCurrencyAmount.text = item.transaction.buyAmount
                    .toCurrencyString(item.buyCurrency.symbol)
                sellCurrencyAmount.text = item.transaction.sellAmount
                    .toCurrencyString(item.sellCurrency.symbol)
            }
        }

        override fun setViewActivated(isActivated: Boolean) {
            binding.transactionContentView.isActivated = isActivated
        }
    }
}