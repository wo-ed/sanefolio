package com.woed.sanefolio.ui.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.woed.sanefolio.R
import com.woed.sanefolio.storage.ProductivityMode
import com.woed.sanefolio.storage.Settings
import com.woed.sanefolio.repositories.SettingsRepository
import com.woed.sanefolio.storage.Coin
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val settingsRepository: SettingsRepository
) : ViewModel() {

    val settings: LiveData<Settings> = settingsRepository.settings.asLiveData()

    val productivityModeText = settingsRepository.settings.map {
        when (it.productivityMode) {
            ProductivityMode.MIN -> R.string.productivity_min_title
            ProductivityMode.MEDIUM -> R.string.productivity_medium_title
            ProductivityMode.MAX -> R.string.productivity_max_title
            else -> -1
        }
    }.asLiveData()

    val productivityModeDescription = settingsRepository.settings.map {
        when (it.productivityMode) {
            ProductivityMode.MIN -> R.string.productivity_min_text
            ProductivityMode.MEDIUM -> R.string.productivity_medium_text
            ProductivityMode.MAX -> R.string.productivity_max_text
            else -> -1
        }
    }.asLiveData()

    val vsCurrency: LiveData<Coin> = settingsRepository.settings.map { it.vsCurrency }.asLiveData()

    fun setValueInNotificationsEnabled(isEnabled: Boolean) {
        viewModelScope.launch {
            if (settings.value?.isValueInNotificationsEnabled != isEnabled) {
                settingsRepository.updateSettings {
                    it.isValueInNotificationsEnabled = isEnabled
                }
            }
        }
    }

    fun set24HourEnabled(isEnabled: Boolean) {
        viewModelScope.launch {
            if (settings.value?.is24HourEnabled != isEnabled) {
                settingsRepository.updateSettings {
                    it.is24HourEnabled = isEnabled
                }
            }
        }
    }

    fun setVsCurrency(vsCurrency: Coin) {
        viewModelScope.launch {
            if (settings.value?.vsCurrency != vsCurrency) {
                settingsRepository.updateSettings {
                    it.vsCurrency = vsCurrency
                }
            }
        }
    }
}