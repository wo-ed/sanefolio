package com.woed.sanefolio.ui.transactions

import androidx.lifecycle.*
import com.woed.sanefolio.repositories.CryptoRepository
import com.woed.sanefolio.repositories.SettingsRepository
import com.woed.sanefolio.storage.Coin
import com.woed.sanefolio.storage.Transaction
import com.woed.sanefolio.storage.TransactionAndCoins
import com.woed.sanefolio.ui.dateTimeFormatter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import java.time.ZonedDateTime
import javax.inject.Inject

@HiltViewModel
class TransactionsViewModel @Inject constructor(
    private val cryptoRepository: CryptoRepository,
    settingsRepository: SettingsRepository
) : ViewModel() {

    private var _coins = MutableLiveData<List<Coin>>()
    val coins: LiveData<List<Coin>> get() = _coins

    val transactionsAndCoins: LiveData<List<TransactionAndCoins>> =
        cryptoRepository.getTransactionsAndCoins().asLiveData()

    private val _dateTime = MutableLiveData(ZonedDateTime.now().withSecond(0).withNano(0))
    val dateTime: LiveData<ZonedDateTime> get() = _dateTime
    val dateTimeString = dateTime.map { it.format(dateTimeFormatter) }

    private val _sellCurrency = MutableLiveData<Coin?>()
    val sellCurrency: LiveData<Coin?> get() = _sellCurrency

    private val _buyCurrency = MutableLiveData<Coin?>()
    val buyCurrency: LiveData<Coin?> get() = _buyCurrency

    val is24HourEnabled = settingsRepository.settings.map { it.is24HourEnabled }.asLiveData()

    val isLoadingFinished = MutableLiveData(false)

    init {
        tryLoadCoins()
    }

    // TODO Handle no internet connection
    // TODO Enable retry
    fun tryLoadCoins() {
        viewModelScope.launch {
            updateCoins(cryptoRepository.getCoins())
        }
    }

    private fun updateCoins(coins: List<Coin>?) {
        _coins.value = coins
        isLoadingFinished.value = true
    }

    fun updateDateTime(dateTime: ZonedDateTime) {
        _dateTime.value = dateTime
    }

    fun updateSellCurrency(coin: Coin?) {
        _sellCurrency.value = coin
    }

    fun updateBuyCurrency(coin: Coin?) {
        _buyCurrency.value = coin
    }

    fun saveTransaction(transaction: TransactionAndCoins) {
        viewModelScope.launch {
            cryptoRepository.insertOrUpdate(transaction)
        }
    }

    fun deleteTransactions(transactions: List<Transaction>) {
        viewModelScope.launch {
            cryptoRepository.deleteTransactions(transactions)
        }
    }
}