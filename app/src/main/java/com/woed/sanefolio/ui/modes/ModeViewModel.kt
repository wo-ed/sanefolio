package com.woed.sanefolio.ui.modes

import androidx.lifecycle.*
import com.woed.sanefolio.repositories.CryptoRepository
import com.woed.sanefolio.storage.ProductivityMode
import com.woed.sanefolio.repositories.SettingsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ModeViewModel @Inject constructor(
    private val settingsRepository: SettingsRepository,
    cryptoRepository: CryptoRepository
) : ViewModel() {

    val initialAction = settingsRepository.settings.take(1).flatMapLatest { settings ->
        cryptoRepository.getIntervalAlarms().take(1).map { alarms ->
            when {
                settings.productivityMode == null -> InitialAction.SHOW_MODE_SELECTION
                // TODO Requirement in extra function
                !settings.isAlarmsModificationAlwaysEnabled && alarms.none { it.isRepeating } -> InitialAction.SHOW_ALARM_SELECTION
                else -> InitialAction.SHOW_PORTFOLIO
            }
        }
    }.asLiveData()

    fun updateSanityMode(mode: ProductivityMode) {
        viewModelScope.launch {
            settingsRepository.updateSettings {
                it.productivityMode = mode
            }
        }
    }
}