package com.woed.sanefolio.ui.alarms

import androidx.lifecycle.*
import com.woed.sanefolio.repositories.CryptoRepository
import com.woed.sanefolio.repositories.SettingsRepository
import com.woed.sanefolio.storage.IntervalAlarm
import com.woed.sanefolio.ui.dateTimeFormatter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.launch
import java.time.ZonedDateTime
import javax.inject.Inject

@HiltViewModel
class AlarmsViewModel @Inject constructor(
    private val cryptoRepository: CryptoRepository,
    settingsRepository: SettingsRepository
) : ViewModel() {

    val alarms: LiveData<List<IntervalAlarm>> = cryptoRepository.getIntervalAlarms().asLiveData()

    private val _dateTime = MutableLiveData(ZonedDateTime.now().withSecond(0).withNano(0))
    val dateTime: LiveData<ZonedDateTime> get() = _dateTime
    val dateTimeString = dateTime.map { it.format(dateTimeFormatter) }

    val is24HourEnabled = settingsRepository.settings.map { it.is24HourEnabled }.asLiveData()

    var isEditOnce = false

    val isModificationEnabled = settingsRepository.settings.take(1).map { settings ->
        // TODO Requirement in extra function
        settings.isAlarmsModificationAlwaysEnabled || isEditOnce
    }.asLiveData()

    val isSaveButtonVisible = alarms.map { alarms ->
        // TODO Requirement in extra function
        alarms.any { it.isRepeating } && isEditOnce
    }

    fun updateDateTime(dateTime: ZonedDateTime) {
        _dateTime.value = dateTime
    }

    fun saveAlarm(alarm: IntervalAlarm) {
        viewModelScope.launch {
            cryptoRepository.scheduleAlarm(alarm)
        }
    }

    fun deleteAlarms(alarms: List<IntervalAlarm>) {
        viewModelScope.launch {
            cryptoRepository.deleteAlarms(alarms)
        }
    }
}