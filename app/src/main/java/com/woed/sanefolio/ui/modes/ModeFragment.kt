package com.woed.sanefolio.ui.modes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.woed.sanefolio.databinding.FragmentModeBinding
import com.woed.sanefolio.storage.ProductivityMode
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ModeFragment : Fragment() {

    private var _binding: FragmentModeBinding? = null
    private val binding get() = _binding!!

    private val viewModel: ModeViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentModeBinding.inflate(layoutInflater)

        viewModel.initialAction.observe(viewLifecycleOwner) { initialAction ->
            when (initialAction) {
                InitialAction.SHOW_ALARM_SELECTION -> showAlarms()
                InitialAction.SHOW_PORTFOLIO -> showPortfolio()
                else -> showView()
            }
        }

        binding.mode1.setOnClickListener {
            viewModel.updateSanityMode(ProductivityMode.MAX)
            showAlarms()
        }

        binding.mode2.setOnClickListener {
            viewModel.updateSanityMode(ProductivityMode.MEDIUM)
            showPortfolio()
        }

        binding.mode3.setOnClickListener {
            viewModel.updateSanityMode(ProductivityMode.MIN)
            showPortfolio()
        }

        return binding.root
    }

    private fun showView() {
        binding.modeViewContainer.visibility = View.VISIBLE
    }

    private fun showPortfolio() {
        val action = ModeFragmentDirections.actionNavModesToNavPortfolio()
        findNavController().navigate(action)
    }

    private fun showAlarms() {
        val action = ModeFragmentDirections.actionNavModesToNavAlarms(
            isFullscreen = true,
            isEditOnce = true
        )
        findNavController().navigate(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}