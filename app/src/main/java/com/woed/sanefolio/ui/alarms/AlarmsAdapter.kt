package com.woed.sanefolio.ui.alarms

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.woed.sanefolio.R
import com.woed.sanefolio.databinding.ListItemAlarmBinding
import com.woed.sanefolio.databinding.ListItemAssetBinding
import com.woed.sanefolio.storage.IntervalAlarm
import com.woed.sanefolio.ui.IdItemListAdapter
import com.woed.sanefolio.ui.dateTimeFormatter

class AlarmsAdapter(
    private val context: Context
) : IdItemListAdapter<IntervalAlarm, AlarmsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ListItemAlarmBinding>(
            LayoutInflater.from(context),
            R.layout.list_item_alarm,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    inner class ViewHolder(private val binding: ListItemAlarmBinding) :
        IdItemListAdapter.ViewHolder<IntervalAlarm>(binding.root) {

        override fun bind(item: IntervalAlarm, isActivated: Boolean) {
            super.bind(item, isActivated)

            val intervalVisibility = if (item.isRepeating) View.VISIBLE else View.GONE

            binding.apply {
                alarmTextView.text = item.nextDateTime.format(dateTimeFormatter)
                repeatButton.visibility = intervalVisibility
                intervalDaysView.text = item.intervalDays.toString()
                intervalDaysView.visibility = intervalVisibility
            }
        }
    }
}