package com.woed.sanefolio.ui.portfolio

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.woed.sanefolio.repositories.SettingsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@HiltViewModel
class AssetItemViewModel @Inject constructor(
    settingsRepository: SettingsRepository
) : ViewModel() {
    val isPortfolioValueVisible: LiveData<Boolean> =
        settingsRepository.settings.map { it.isPortfolioValueVisible }.asLiveData()
    val vsCurrencySymbol: LiveData<String> =
        settingsRepository.settings.map { it.vsCurrency.symbol }.asLiveData()
}
