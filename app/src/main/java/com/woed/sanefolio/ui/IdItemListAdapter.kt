package com.woed.sanefolio.ui

import android.view.View
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

interface LongIdItem {
    val longId: Long
    override fun equals(other: Any?): Boolean
}

class DiffUtilIdItemCallback<T> : DiffUtil.ItemCallback<T>() where T : LongIdItem {
    override fun areItemsTheSame(old: T, new: T) = old.longId == new.longId
    override fun areContentsTheSame(old: T, new: T) = old == new
}

abstract class IdItemListAdapter<T, VH>(itemDiffUtil: DiffUtil.ItemCallback<T> = DiffUtilIdItemCallback()) :
    ListAdapter<T, VH>(itemDiffUtil) where T : LongIdItem, VH : IdItemListAdapter.ViewHolder<T> {

    var selectionTracker: SelectionTracker<Long>? = null

    init {
        initialize()
    }

    private fun initialize() {
        setHasStableIds(true)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val item = getItem(position)
        holder.bind(item, selectionTracker?.isSelected(item?.longId) ?: false)
    }

    override fun getItemCount() = currentList.size

    override fun getItemId(position: Int) = getItem(position).longId

    open class ViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var item: T? = null

        open fun bind(item: T, isActivated: Boolean = false) {
            this.item = item
            setViewActivated(isActivated)
        }

        open fun setViewActivated(isActivated: Boolean) {
            itemView.isActivated = isActivated
        }
    }
}