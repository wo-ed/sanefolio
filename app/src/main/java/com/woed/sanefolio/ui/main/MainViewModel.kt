package com.woed.sanefolio.ui.main

import androidx.lifecycle.ViewModel
import com.woed.sanefolio.repositories.CryptoRepository
import com.woed.sanefolio.repositories.SettingsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    settingsRepository: SettingsRepository,
    cryptoRepository: CryptoRepository
) : ViewModel() {
}