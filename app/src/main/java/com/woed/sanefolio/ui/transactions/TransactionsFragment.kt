package com.woed.sanefolio.ui.transactions

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.woed.sanefolio.R
import com.woed.sanefolio.databinding.FragmentTransactionsBinding
import com.woed.sanefolio.filterCoinsSorted
import com.woed.sanefolio.storage.Coin
import com.woed.sanefolio.storage.Transaction
import com.woed.sanefolio.storage.TransactionAndCoins
import com.woed.sanefolio.ui.*
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class TransactionsFragment : Fragment() {

    private val viewModel: TransactionsViewModel by viewModels()

    private var _binding: FragmentTransactionsBinding? = null
    private val binding get() = _binding!!
    private val args: TransactionsFragmentArgs by navArgs()

    private lateinit var actionModeManager: ActionModeManager<Transaction>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_transactions, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        val transactionsAdapter = TransactionsAdapter(requireContext())
        binding.transactionsView.adapter = transactionsAdapter
        binding.transactionsView.layoutManager = LinearLayoutManager(requireContext())

        val selectionTracker = longSelectionTrackerBuilder(
            SELECTION_TRANSACTIONS, binding.transactionsView
        ).build().also { transactionsAdapter.selectionTracker = it }

        actionModeManager =
            ActionModeManager(object : ActionModeManager.Provider<Transaction> {
                override val selectionTracker: SelectionTracker<Long> get() = selectionTracker
                override fun getActivity() = this@TransactionsFragment.activity

                override fun getAllItems() =
                    viewModel.transactionsAndCoins.value?.map { it.transaction } ?: emptyList()

                override fun getItemById(id: Long): Transaction? =
                    viewModel.transactionsAndCoins.value?.map { it.transaction }
                        ?.firstOrNull { it.longId == id }

                override fun deleteItems(items: List<Transaction>) {
                    viewModel.deleteTransactions(items)
                }
            }).apply { create() }

        binding.submitButton.setOnClickListener {
            val transaction = createTransactionFromInput()

            transaction?.let { viewModel.saveTransaction(it) }
        }

        binding.dateView.setOnClickListener {
            val initialDate = viewModel.dateTime.value!!
            // TODO 24Hour false doesn't work
            requireContext().dateTimePicker(initialDate, viewModel.is24HourEnabled.value ?: true) {
                viewModel.updateDateTime(it)
            }
        }

        transactionsAdapter.registerAdapterDataObserver(
            object : RecyclerView.AdapterDataObserver() {
                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    binding.transactionsView.scrollToPosition(positionStart)
                }
            }
        )

        viewModel.transactionsAndCoins.observe(viewLifecycleOwner) { transactionsAndCoins ->
            transactionsAdapter.submitList(transactionsAndCoins)
        }

        binding.buyAmountInput.onlyPositiveDecimals()
        binding.sellAmountInput.onlyPositiveDecimals()

        val autoCompleteAdapter = AutoCompleteCoinAdapter(requireContext())
        binding.buyCurrencyInput.setAdapter(autoCompleteAdapter)
        binding.sellCurrencyInput.setAdapter(autoCompleteAdapter)

        viewModel.coins.observe(viewLifecycleOwner) {
            // TODO: Update filter
            autoCompleteAdapter.allCoins = it
        }

        binding.buyCurrencyInput.setOnItemClickListener { adapterView, _, position, _ ->
            val coin = adapterView.getItemAtPosition(position) as? Coin
            viewModel.updateBuyCurrency(coin)
            binding.buyCurrencyInput.apply {
                binding.root.findViewById<View>(nextFocusDownId)?.requestFocus()
            }
        }

        binding.sellCurrencyInput.setOnItemClickListener { adapterView, _, position, _ ->
            val coin = adapterView.getItemAtPosition(position) as? Coin
            viewModel.updateSellCurrency(coin)
            binding.sellCurrencyInput.apply {
                clearFocus()
            }
        }

        binding.buyCurrencyInput.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus && viewModel.buyCurrency.value == null) {
                selectBuyCurrencyByQuery()
            } else if (hasFocus) {
                resetBuyCurrency()
            }
        }

        binding.sellCurrencyInput.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus && viewModel.sellCurrency.value == null) {
                selectSellCurrencyByQuery()
            } else if (hasFocus) {
                resetSellCurrency()
            }
        }

        /*binding.buyAmountInput.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                binding.buyAmountInput.text.clear()
            }
        }

        binding.sellAmountInput.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                binding.sellAmountInput.text.clear()
            }
        }*/

        binding.sellCurrencyInput.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (binding.sellCurrencyInput.hasFocus()) {
                    val coin = getCoinByQuery(binding.sellCurrencyInput.text.toString())
                    viewModel.updateSellCurrency(coin)
                    if (coin != null) {
                        binding.sellCurrencyInput.clearFocus()
                    }
                } else {
                    binding.submitButton.performClick()
                }
                true
            } else false
        }

        binding.buyCurrencyButton.setOnClickListener {
            resetBuyCurrency()
            binding.buyCurrencyInput.requestFocus()
        }

        binding.sellCurrencyButton.setOnClickListener {
            resetSellCurrency()
            binding.sellCurrencyInput.requestFocus()
        }

        if (args.initialFocus) {
            // TODO Show keyboard not working
            /*binding.buyAmountInput.requestFocus()
            activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
            val imm = getSystemService(requireContext(), InputMethodManager::class.java)
            imm?.showSoftInput(binding.buyAmountInput, InputMethodManager.SHOW_FORCED)*/
        }

        /*
        CoinsSyncWorker.getWorkInfosForUniqueWorkLiveData(requireContext())
            .observe(viewLifecycleOwner) {
                val isWorkerRunning = it.any { workInfo ->
                    workInfo.state == WorkInfo.State.RUNNING
                }

                binding.coinsSyncProgressView.visibility =
                    if (isWorkerRunning) View.VISIBLE else View.GONE
            }*/

        viewModel.isLoadingFinished.observe(viewLifecycleOwner) {
            viewLifecycleOwner.lifecycleScope.launch {

                // Show the ProgressBar for a moment because it looks bad when hidden after a very short time
                delay(1000)

                binding.coinsSyncProgressView.visibility = View.GONE
            }
        }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        actionModeManager.destroy()
        _binding = null
    }

    private fun resetSellCurrency() {
        viewModel.updateSellCurrency(null)
        //binding.sellCurrencyInput.text.clear()
    }

    private fun resetBuyCurrency() {
        viewModel.updateBuyCurrency(null)
        //binding.buyCurrencyInput.text.clear()
    }

    private fun selectBuyCurrencyByQuery() {
        val coin = getCoinByQuery(binding.buyCurrencyInput.text.toString())
        viewModel.updateBuyCurrency(coin)
    }

    private fun selectSellCurrencyByQuery() {
        val coin = getCoinByQuery(binding.sellCurrencyInput.text.toString())
        viewModel.updateSellCurrency(coin)
    }

    private fun getCoinByQuery(input: String, position: Int = 0): Coin? =
        filterCoinsSorted(input, viewModel.coins.value ?: emptyList()).getOrNull(position)

    private fun createTransactionFromInput(): TransactionAndCoins? {
        if (viewModel.buyCurrency.value == null) {
            selectBuyCurrencyByQuery()
        }

        if (viewModel.sellCurrency.value == null) {
            selectSellCurrencyByQuery()
        }

        val sellAmount = binding.sellAmountInput.text.toString().toBigDecimalOrNull()
        val sellCurrency = viewModel.sellCurrency.value
        val buyAmount = binding.buyAmountInput.text.toString().toBigDecimalOrNull()
        val buyCurrency = viewModel.buyCurrency.value
        val timestamp = viewModel.dateTime.value

        return if (sellAmount != null && sellCurrency != null && buyAmount != null && buyCurrency != null && timestamp != null) {

            binding.buyAmountInput.requestFocus()
            binding.buyAmountInput.text.clear()
            binding.sellAmountInput.text.clear()
            resetBuyCurrency()
            resetSellCurrency()
            binding.buyCurrencyInput.text.clear()
            binding.sellCurrencyInput.text.clear()

            TransactionAndCoins(
                Transaction(
                    buyAmount, 0L,
                    sellAmount, 0L,
                    timestamp
                ), buyCurrency, sellCurrency
            )
        } else null
    }
}
