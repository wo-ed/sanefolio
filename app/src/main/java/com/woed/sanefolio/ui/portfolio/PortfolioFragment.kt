package com.woed.sanefolio.ui.portfolio

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.appbar.AppBarLayout
import com.woed.sanefolio.Asset
import com.woed.sanefolio.R
import com.woed.sanefolio.databinding.FragmentPortfolioBinding
import com.woed.sanefolio.ui.ActionModeManager
import com.woed.sanefolio.ui.SELECTION_ASSETS
import com.woed.sanefolio.ui.longSelectionTrackerBuilder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PortfolioFragment : Fragment() {

    private val viewModel: PortfolioViewModel by viewModels()
    private var _binding: FragmentPortfolioBinding? = null
    private val binding get() = _binding!!

    private lateinit var actionModeManager: ActionModeManager<Asset>

    private var isSecondAppBarExpanded = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_portfolio, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        val assetsAdapter = AssetsAdapter(inflater.context, viewLifecycleOwner, this)
        binding.assetsListView.adapter = assetsAdapter
        binding.assetsListView.layoutManager = LinearLayoutManager(context)

        val selectionTracker = longSelectionTrackerBuilder(
            SELECTION_ASSETS, binding.assetsListView
        ).build().also { assetsAdapter.selectionTracker = it }

        actionModeManager = ActionModeManager(object : ActionModeManager.Provider<Asset> {
            override val selectionTracker: SelectionTracker<Long> get() = selectionTracker
            override fun getActivity() = this@PortfolioFragment.activity
            override fun getAllItems() = viewModel.assets.value ?: emptyList()
            override fun getItemById(id: Long): Asset? =
                viewModel.assets.value?.firstOrNull { it.longId == id }

            override fun deleteItems(items: List<Asset>) {
                viewModel.deleteAssets(items)
            }
        }).apply { create() }

        actionModeManager.create()

        viewModel.assets.observe(viewLifecycleOwner) { assets ->
            assetsAdapter.submitList(assets)
            binding.swipeRefresh.isRefreshing = false
        }

        binding.visibilityToggle.setOnClickListener { viewModel.togglePortfolioValueVisibility() }

        binding.fab.setOnClickListener {
            val action = PortfolioFragmentDirections.actionNavPortfolioToNavNewTransaction(true)
            findNavController().navigate(action)
        }

        binding.swipeRefresh.setOnRefreshListener {
            viewModel.synchronizePricesIfAllowed()
        }

        viewModel.isManualPricesSyncAllowed.observe(viewLifecycleOwner) {
            updateSwipeRefreshState()
        }

        binding.secondAppBar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
            isSecondAppBarExpanded = verticalOffset == 0
            updateSwipeRefreshState()
        })

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        actionModeManager.destroy()
        _binding = null
    }

    // TODO Find a better way
    private fun updateSwipeRefreshState() {
        binding.swipeRefresh.isEnabled =
            viewModel.isManualPricesSyncAllowed.value ?: false && isSecondAppBarExpanded
    }
}