package com.woed.sanefolio.ui

import android.app.Activity
import android.view.ActionMode
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.recyclerview.selection.SelectionTracker
import com.woed.sanefolio.R

class ActionModeManager<T>(private val provider: Provider<T>) :
    ActionMode.Callback where T : LongIdItem {

    private val selectionTracker get() = provider.selectionTracker
    private var actionMode: ActionMode? = null

    fun create() {
        selectionTracker.addObserver(object : SelectionTracker.SelectionObserver<Long>() {
            override fun onSelectionChanged() {
                if (selectionTracker.selection.isEmpty) {
                    actionMode?.finish()

                    // Hack needed to deselect every item
                    // TODO: Remove hack
                    // TODO: Reimplement
                    //binding.assetsListView.adapter?.notifyDataSetChanged()
                } else if (actionMode == null) {
                    actionMode = provider.getActivity()?.startActionMode(this@ActionModeManager)
                }
            }
        })
    }

    fun destroy() {
        actionMode?.finish()
        actionMode = null
    }

    override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
        val inflater: MenuInflater = mode.menuInflater
        inflater.inflate(R.menu.contextual, menu)
        return true
    }

    override fun onPrepareActionMode(mode: ActionMode, menu: Menu) = false

    override fun onActionItemClicked(mode: ActionMode, item: MenuItem) =
        when (item.itemId) {
            R.id.action_deselect -> {
                provider.selectionTracker.clearSelection()
                true
            }
            R.id.action_select_all -> {
                provider.getAllItems().forEach {
                    provider.selectionTracker.select(it.longId)
                }
                true
            }
            R.id.action_delete -> {
                val selectedItems = provider.selectionTracker.selection.mapNotNull { id ->
                    provider.getItemById(id)
                }
                provider.deleteItems(selectedItems)
                actionMode?.finish()
                true
            }
            else -> false
        }

    override fun onDestroyActionMode(mode: ActionMode) {
        actionMode = null
        provider.selectionTracker.clearSelection()
    }

    interface Provider<T> where T : LongIdItem {
        val selectionTracker: SelectionTracker<Long>
        fun getActivity(): Activity?
        fun getAllItems(): List<T>
        fun getItemById(id: Long): T?
        fun deleteItems(items: List<T>)
    }
}