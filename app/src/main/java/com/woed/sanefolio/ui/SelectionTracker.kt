package com.woed.sanefolio.ui

import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.RecyclerView

private const val DEFAULT_SCOPE = ItemKeyProvider.SCOPE_MAPPED

fun longSelectionTrackerBuilder(
    selectionId: String,
    recyclerView: RecyclerView,
    scope: Int = DEFAULT_SCOPE,
) = SelectionTracker.Builder(
    selectionId,
    recyclerView,
    LongItemKeyProvider(recyclerView, scope),
    LongItemDetailsLookup(recyclerView),
    StorageStrategy.createLongStorage()
)

class LongItemDetailsLookup(private val recyclerView: RecyclerView) : ItemDetailsLookup<Long>() {
    override fun getItemDetails(e: MotionEvent) =
        recyclerView.findChildViewUnder(e.x, e.y)?.let { view ->
            object : ItemDetailsLookup.ItemDetails<Long>() {
                override fun getPosition() = recyclerView.getChildViewHolder(view)?.adapterPosition
                    ?: RecyclerView.NO_POSITION

                override fun getSelectionKey() =
                    recyclerView.getChildViewHolder(view)?.itemId ?: RecyclerView.NO_ID
            }
        }
}

class LongItemKeyProvider(
    private val recyclerView: RecyclerView,
    scope: Int = DEFAULT_SCOPE,
) : ItemKeyProvider<Long>(scope) {

    override fun getKey(position: Int) =
        recyclerView.adapter?.getItemId(position) ?: RecyclerView.NO_ID

    override fun getPosition(key: Long) =
        recyclerView.findViewHolderForItemId(key)?.layoutPosition ?: RecyclerView.NO_POSITION
}