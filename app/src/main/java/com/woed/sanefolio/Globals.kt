package com.woed.sanefolio

const val ACTION_ALARM = "com.woed.sanefolio.intent.action.ALARM"
const val ACTION_PRICES_SYNC_RETRY = "com.woed.sanefolio.intent.action.PRICES_SYNC"

const val REQUEST_CODE_RETRY = 1
const val REQUEST_CODE_START_MAIN = 2
const val REQUEST_CODE_ALARM = 100000000
// ALARM 1: 100000001
// ALARM 2: 100000002
// ...